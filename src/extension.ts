// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log(
    'Congratulations, your extension "obsidian-share" is now active!'
  );

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  let disposable = vscode.commands.registerTextEditorCommand(
    "obsidian-share.share",
    (editor, edit, args) => {
      const vaultPath: string | undefined = vscode.workspace
        .getConfiguration("obsidianShare")
        .get("vault");
      if (!vaultPath || vaultPath === "") {
        vscode.window.showErrorMessage("Vault path not set in settings");
        return;
      }

      const selection = editor?.selection;
      if (!selection || selection.isEmpty) {
        vscode.window.showInformationMessage("Nothing selected");
        return;
      }

      const selectionRange = new vscode.Range(
        selection.start.line,
        selection.start.character,
        selection.end.line,
        selection.end.character
      );
      const highlighted = editor.document.getText(selectionRange);

      const content = `\`\`\`\n${highlighted}\n\`\`\`\n\n<${editor.document.uri.toString()}>`;
      const contentEncoded = encodeURIComponent(content);

      const title = vscode.window.showInputBox({
        prompt: "Enter note title",
      });
      const vaultPathEncoded = encodeURIComponent(vaultPath);
      title.then((title) => {
        if (!title || title === "") {
          vscode.window.showErrorMessage("No title provided");
          return;
        }
        const titleEncoded = encodeURIComponent(title);
        const params = `vault=${vaultPathEncoded}&name=${titleEncoded}&content=${contentEncoded}`;
        const uri = vscode.Uri.from({
          scheme: "obsidian",
          authority: "new",
        });
        vscode.env.openExternal(vscode.Uri.parse(`${uri}?${params}`)).then(
          () => {
            console.log("successfully invoked obsidian");
          },
          (err) => {
            console.log(err);
            vscode.window.showErrorMessage("Failed to invoke Obsidian");
          }
        );
      });
    }
  );

  context.subscriptions.push(disposable);
}
