# obsidian-share README

This is a simple extension that can yeet selected text, with some simple context into Obsidian as a new note

## Features

The only feature in this is a command, `Obsidian: Share`, which takes the current selection and creates it as a new note using [Obsidian URIs](https://help.obsidian.md/Concepts/Obsidian+URI)

## Requirements

The only requirement is Obsidian, duh. Follow the instructions on [Registering Obsidian URIs](https://help.obsidian.md/Concepts/Obsidian+URI#Register+Obsidian+URI)

## Extension Settings

This extension contributes the following settings:

* `obsidianShare.vault`: Either the Vault Name or ID to create notes in

## Known Issues

Calling out known issues can help limit users opening duplicate issues against your extension.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of ...

### 1.0.1

Fixed issue #.

### 1.1.0

Added features X, Y, and Z.

---

## Following extension guidelines

Ensure that you've read through the extensions guidelines and follow the best practices for creating your extension.

* [Extension Guidelines](https://code.visualstudio.com/api/references/extension-guidelines)

## Working with Markdown

You can author your README using Visual Studio Code. Here are some useful editor keyboard shortcuts:

* Split the editor (`Cmd+\` on macOS or `Ctrl+\` on Windows and Linux).
* Toggle preview (`Shift+Cmd+V` on macOS or `Shift+Ctrl+V` on Windows and Linux).
* Press `Ctrl+Space` (Windows, Linux, macOS) to see a list of Markdown snippets.

## For more information

* [Visual Studio Code's Markdown Support](http://code.visualstudio.com/docs/languages/markdown)
* [Markdown Syntax Reference](https://help.github.com/articles/markdown-basics/)

**Enjoy!**
